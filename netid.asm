%include "asm_io.inc"
;
; initialized data is put in the .data segment
;
segment .data
;
; These labels refer to strings used for output
;
prompt1 db    "IP-part: ", 0       ; don't forget nul terminator
outmsg1 db    "NetID:  ", 0
msg1	db 	".",0
msg2 	db	"Präfix: ",0
;
; uninitialized data is put in the .bss segment
;
segment .bss
;
; These labels refer to double words used to store the inputs
;
input1  resd 1
input2  resd 1

inshift resd 1

;
; code is put in the .text segment
;
segment .text
        global  asm_main
asm_main:
        enter   0,0               ; setup routine
        pusha
	

	mov 	eax, prompt1
	call print_string

	call read_int
	shr	ebx, 8
	shl	ebx, 8
	mov 	ebx, eax
	shl	ebx, 8

	mov 	eax, prompt1
	call print_string
	call read_int
	add 	ebx,eax
	shl 	ebx, 8

	mov 	eax, prompt1
	call print_string
	call read_int
	add 	ebx, eax
	shl 	ebx, 8

	mov 	eax, prompt1
	call print_string
	call read_int
	add 	ebx,eax

	mov eax, msg2
	call print_string

	
	call read_int
	mov [inshift],eax
	mov 	cl, [inshift]
	shr 	ebx, cl  
	shl	ebx, cl 

	mov 	eax, outmsg1
	call print_string
	mov 	eax,ebx
	shr	eax, 24 
	
	call print_int
	
	mov eax, msg1
	call print_string
	mov eax, ebx
	shl eax,8	
	shr eax,24
	call print_int
	
	mov eax, msg1
	call print_string
	mov eax, ebx
	shl eax, 16
	shr eax, 24
	
	call print_int
	
	mov eax, msg1
	call print_string
	mov eax, ebx
	shl eax, 24
	shr eax, 24
	call print_int		
    call print_nl
    popa
    mov     eax, 0            ; return back to C
    leave                     
    ret