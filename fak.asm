%include "asm_io.inc"
;
; initialized data is put in the .data segment
;
segment .data
;
; These labels refer to strings used for output
;
prompt1 db    "Fakultat von: ", 0       ; don't forget nul terminator
outmsg1 db    "Ergebnis:  ", 0
;
; uninitialized data is put in the .bss segment
;
segment .bss
;
; These labels refer to double words used to store the inputs
;
input1  resd 1
input2  resd 1

inshift resd 1

;
; code is put in the .text segment
;
segment .text
        global  asm_main
asm_main:
        enter   0,0               ; setup routine
        pusha
	

	mov	eax, prompt1
	call 	print_string
	call	read_int

	mov	ecx, eax

loop:
	cmp	ecx, 1
	jle	break

	sub	ecx, 1
	mul	ecx
	jmp 	loop
	
break:
	call	print_int

        popa
        mov     eax, 0            ; return back to C
        leave                     
        ret


