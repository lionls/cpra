PRAGMA auto_vauum = 1;
PRAGMA encoding = "UTF-8";
PRAGMA foreign_keys = 1;
PRAGMA journal_mode = WAL;
PRAGMA synchronous = NORMAL;

CREATE TABLE movie(
  id INT NOT NULL,
  description text NOT NULL CHECK (description <> '' AND description NOT GLOB '*[^ -~]*'),
  FSK INT NOT NULL CHECK (FSK IN (0, 6, 12, 16, 18)),
  title VARCHAR NOT NULL CHECK (title <> '' AND title NOT GLOB '*[^ -~]*'),
  relDate DATE NOT NULL CHECK(date(relDate) == relDate),
  length INT NOT NULL,
  coverImg BLOB,
  trailerID VARCHAR CHECK (trailerID LIKE 'https://www.youtube.com/embed/%' ), /*special check*/
  PRIMARY key(id)
);
CREATE TABLE room(
  name VARCHAR NOT NULL CHECK (name <> '' AND name NOT GLOB '*[^ -~]*'),
  FLOOR INT NOT NULL,
  description text NOT NULL CHECK (description <> '' AND description NOT GLOB '*[^ -~]*'),
  PRIMARY key(name)
);
CREATE TABLE presentation(
  id INT NOT NULL,
  LANGUAGE VARCHAR NOT NULL CHECK (LANGUAGE <> '' AND LANGUAGE NOT GLOB '*[^ -~]*'),
  date DATE NOT NULL CHECK(date(date) == date AND cast(strftime('%w', date)as INT)>=1 AND cast(strftime('%w', date)as int) <=5), /*date < relDATE in TRIGGER */
  time TIME NOT NULL CHECK(time(time) == time AND time <= time("24:00:00") AND time >= time("18:00:00")),
  is3D BOOLEAN NOT NULL,
  roomName VARCHAR NOT NULL CHECK (roomName <> '' AND roomName NOT GLOB '*[^ -~]*'),
  movieID INT NOT NULL,
  PRIMARY key(id),
  FOREIGN KEY (roomName) REFERENCES room(name) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (movieID) REFERENCES movie(id) ON UPDATE CASCADE ON DELETE CASCADE
  /*CHECK DATE > relDATE
   CHECK DATE IS WEEKDAY
   CHECK TIME between 18:00,24:00*/
);
CREATE TABLE ticket(
  id INT NOT NULL,
  ROW CHAR(1) NOT NULL CHECK (ROW <> '' and ROW NOT GLOB '*[^ -~]*'),
  place INT NOT NULL CHECK(place>0 AND place<21),
  children BOOLEAN,
  presentationID,
  FOREIGN key (presentationID) REFERENCES presentation(id) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY key(id)
/*check if row is between A-Z
 on user delete delete ticket cascade
nochmal lesen
 */
);
CREATE TABLE price(
  id VARCHAR NOT NULL,
  VALUE UNSIGNED DECIMAL(10,2) NOT NULL, /*/////// GEHT NICHT ///*/
  PRIMARY key(id)
);

INSERT INTO price (id,VALUE) VALUES ("base",8),("overtime",2),("3d",3),("children",-4); /* hier eingefügt da Tabelle konstant ist */

CREATE TABLE genre(
  name VARCHAR NOT NULL CHECK (name <> '' AND name NOT GLOB '*[^ -~]*'),
  PRIMARY key(name)
);
CREATE TABLE person(
  id INT NOT NULL,
  firstname VARCHAR NOT NULL CHECK (firstname <> '' AND firstname NOT GLOB '*[^ -~]*'),
  lastname VARCHAR NOT NULL CHECK (lastname <> '' AND lastname NOT GLOB '*[^ -~]*'),
  birthdate DATE CHECK (date(birthdate) == birthdate ),
  PRIMARY key(id)
);
CREATE TABLE user(
  email VARCHAR NOT NULL CHECK (email LIKE '%_@__%.__%'), /*check email format*/
  password VARCHAR NOT NULL CHECK (password <> '' AND password NOT GLOB '*[^ -~]*'),
  personID INT NOT NULL,
  PRIMARY key(email),
  FOREIGN key(personID) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE
/* CHECK AGE >18*/
);
CREATE TABLE ratings(
  movieID INT NOT NULL,
  userEmail VARCHAR CHECK (userEmail <> '' AND userEmail NOT GLOB '*[^ -~]*'), /* CAN BE NULL TO ANOMIZE */
  stars INT NOT NULL CHECK(stars>0 AND stars <=10),
  FOREIGN key (movieID) REFERENCES movie(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key (userEmail) REFERENCES USER(email) ON UPDATE CASCADE ON DELETE CASCADE,
  PRIMARY key(movieID, userEmail)
/* check stars between 1-10
 DO NOT DELETE ON CASCADE ---> anonymize rating
 */
);
CREATE TABLE image(
  id INT NOT NULL,
  BLOB BLOB NOT NULL,
  roomName VARCHAR NOT NULL CHECK (roomName <> '' AND roomName NOT GLOB '*[^ -~]*'),
  PRIMARY key(id),
  FOREIGN key (roomName) REFERENCES room(Name) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE actor(
  personID INT NOT NULL,
  specialname VARCHAR CHECK (specialName <> '' AND specialName NOT GLOB '*[^ -~]*'),
  PRIMARY key(personID),
  FOREIGN key(personID) REFERENCES person(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE workplace(
  name VARCHAR NOT NULL CHECK (name <> '' AND name NOT GLOB '*[^ -~]*'),
  PRIMARY key(name)
);
CREATE TABLE worker(
  userEmail VARCHAR NOT NULL CHECK (userEmail <> '' AND userEmail NOT GLOB '*[^ -~]*'),
  workplaceName VARCHAR NOT NULL CHECK (workplaceName <> '' AND workplaceName NOT GLOB '*[^ -~]*'),
  PRIMARY key(userEmail),
  FOREIGN key(userEmail) REFERENCES USER(email) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key(workplaceName) REFERENCES workplace(name) ON UPDATE CASCADE ON DELETE CASCADE
/* ROW BASED SECURITY??*/
);
CREATE TABLE admin(
  mobilephone VARCHAR NOT NULL CHECK (mobilephone <> '' AND mobilephone GLOB '*[+0-9]*' AND length(mobilephone) > 5), /*CHECK NOT WORKING */
  workerUserEmail VARCHAR NOT NULL,
  PRIMARY key(mobilephone),
  FOREIGN key(workerUserEmail) REFERENCES worker(userEmail) ON UPDATE CASCADE ON DELETE CASCADE
  /* +491276555555555 */
);
CREATE TABLE ticket_has_price(
  priceID VARCHAR NOT NULL,
  ticketID INT NOT NULL,
  PRIMARY key (priceID, ticketID),
  FOREIGN key (ticketID) REFERENCES ticket(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key (priceID) REFERENCES price(id) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE movie_belongs_to_genre(
  movieID INT NOT NULL,
  genreName INT NOT NULL,
  PRIMARY key (movieID, genreName),
  FOREIGN key (movieID) REFERENCES movie(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key (genreName) REFERENCES genre(name) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE actor_in_movie(
  movieID INT NOT NULL,
  actorPersonID INT NOT NULL,
  PRIMARY key (movieID, actorPersonID),
  FOREIGN key (movieID) REFERENCES movie(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key (actorPersonID) REFERENCES actor(personID) ON UPDATE CASCADE ON DELETE CASCADE
);
CREATE TABLE user_reserves_ticket(
  ticketID INT NOT NULL,
  userEmail INT NOT NULL,
  PRIMARY key (ticketID),
  FOREIGN key (ticketID) REFERENCES ticket(id) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN key (userEmail) REFERENCES USER(email) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TRIGGER prevent_presentation_date_insert
         AFTER INSERT
            ON presentation
          WHEN EXISTS (
    SELECT *
      FROM movie m,
           presentation p
     WHERE m.id = p.movieID AND
           m.relDate >= p.date
)
BEGIN
    SELECT RAISE(ABORT, "Date can not be lower than release date.") [ROLLBACK];
END;
CREATE TRIGGER prevent_presentation_date_update
         AFTER UPDATE
            ON presentation
          WHEN EXISTS (
    SELECT *
      FROM movie m,
           presentation p
     WHERE m.id = p.movieID AND
           m.relDate >= p.date
)
BEGIN
    SELECT RAISE(ABORT, "Date can not be lower than release date.") [ROLLBACK];
END;


CREATE TRIGGER on_User_delete
        BEFORE DELETE
            ON USER
BEGIN
    UPDATE ratings
       SET userEmail = NULL
     WHERE userEmail = old.email;
END;

CREATE TRIGGER ticket_add_price_base
         AFTER INSERT
            ON ticket
BEGIN
    INSERT INTO ticket_has_price (
                                     priceID,
                                     ticketID
                                 )
                                 VALUES (
                                     "base",
                                     new.id
                                 );
END;


CREATE TRIGGER ticket_add_price_3D
         AFTER INSERT
            ON ticket
          WHEN          1 == (SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                             FROM presentation p
                            WHERE p.id = new.presentationID AND p.is3D == 1)

BEGIN
    INSERT INTO ticket_has_price (
                                     priceID,
                                     ticketID
                                 )
                                 VALUES (
                                     "3d",
                                     new.id
                                 );
END;

CREATE TRIGGER ticket_add_price_children
         AFTER INSERT
            ON ticket
          WHEN new.children == 1
BEGIN
    INSERT INTO ticket_has_price (
                                     priceID,
                                     ticketID
                                 )
                                 VALUES (
                                     "children",
                                     new.id
                                 );
END;




CREATE TRIGGER ticket_add_price_overtime
         AFTER INSERT
            ON ticket
          WHEN 1==(SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                             FROM movie m,
                                  presentation p
                            WHERE m.id = p.movieID AND
                                  p.id = new.presentationID AND
                                  m.length > 120)

BEGIN
    INSERT INTO ticket_has_price (
                                     priceID,
                                     ticketID
                                 )
                                 VALUES (
                                     "overtime",
                                     new.id
                                 );
END;

CREATE TRIGGER admin_already_exists
        BEFORE INSERT
            ON admin
          WHEN 1 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM admin
                    )
BEGIN
    SELECT RAISE(ABORT, "Admin already exists.") [ROLLBACK];
END;

CREATE TRIGGER admin_in_IT
        BEFORE INSERT
            ON admin
          WHEN 0 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM worker w
                         WHERE w.userEmail = new.workerUserEmail AND
                               w.workplaceName = 'IT'
                    )
BEGIN
    SELECT RAISE(ABORT, "Admin must be in IT") [ROLLBACK];
END;

CREATE TRIGGER admin_in_IT_update
        BEFORE UPDATE
            ON admin
          WHEN 0 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM worker w
                         WHERE w.userEmail = new.workerUserEmail AND
                               w.workplaceName = 'IT'
                    )
BEGIN
    SELECT RAISE(ABORT, "Admin must be in IT") [ROLLBACK];
END;

CREATE TRIGGER IT_check_admin
        BEFORE UPDATE
            ON worker
          WHEN 1 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM admin
                         WHERE new.userEmail == workerUserEmail
                    )
BEGIN
    SELECT RAISE(ABORT, "Can not change workplace. Admin must be in IT") [ROLLBACK];
END;

CREATE TRIGGER presentation_at_the_same_damn_time
        BEFORE INSERT
            ON presentation
          WHEN 1 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM presentation p,
                               movie m
                         WHERE p.date == new.date AND
                               p.roomName == new.roomName AND
                               m.id == p.movieID AND
                               (CAST (strftime('%s', p.time) AS INT) + m.length * 60) >= CAST (strftime('%s', new.time) AS INT)
                    )
BEGIN
    SELECT RAISE(ABORT, "Presentation at the same damn time.") [ROLLBACK];
END;

CREATE TRIGGER user_age_over_18
        BEFORE UPDATE
            ON person
          WHEN 0 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM user
                         WHERE user.personID == new.id AND
                               new.birthdate <= date('now', '-18 years')
                    )
BEGIN
    SELECT RAISE(ABORT, "Registered persons must be 18 or older.") [ROLLBACK];
END;

CREATE TRIGGER age_check
        BEFORE INSERT
            ON user
          WHEN 0 == (
                        SELECT CAST (CASE WHEN COUNT( * ) > 0 THEN 1 ELSE 0 END AS BIT)
                          FROM person p
                         WHERE p.id == new.personID AND
                               p.birthdate <= date('now', '-18 years')
                    )
BEGIN
    SELECT RAISE(ABORT, "Registered persons must be 18 or older.") [ROLLBACK];
END;


/* IMAGE ROOM CHECK 3 - RELATION */
